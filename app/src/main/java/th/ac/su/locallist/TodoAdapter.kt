package th.ac.su.locallist

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import th.ac.su.locallist.databinding.RowListBinding
import th.ac.su.locallist.services.Todo

class TodoAdapter(
    private val list: List<Todo>,
    private val onItemSelected: (todo: Todo) -> Unit
) : RecyclerView.Adapter<TodoAdapter.TodoViewHolder>() {

    class TodoViewHolder(private val binding: RowListBinding) : ViewHolder(binding.root) {

        private val context: Context by lazy { binding.root.context }

        /**
         * สำหรับ แสดงข้อมูลของแต่ละ Row หากมีการทำ If ต้องมี else สำหรับ set ข้อมูลใหม่
         * ไม่งั้นอาจเจอบาง Item แสดงข้อมูลเก่า
         */
        fun bind(todo: Todo) {

            binding.txvTodoHeader.text = todo.header
            binding.txvTodoMessage.text = todo.message

            if (todo.picture.isNullOrEmpty()) {
                binding.imvTodoImage.setImageResource(R.drawable.ic_gallery)
            } else {
                val imageUrl = "${context.getString(R.string.base_url)}/${todo.picture}"
                Glide.with(context)
                    .load(imageUrl)
                    .placeholder(R.drawable.ic_download)
                    .error(R.drawable.ic_error)
                    .fitCenter()
                    .into(binding.imvTodoImage)
            }
        }
    }

    /**
     * สำหรับ Binding UI สำหรับสร้าง UI แต่ละ Row
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val binding = RowListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TodoViewHolder(binding)
    }

    /**
     * สำหรับบอกว่า List ที่แสดงใน RecyclerView มีกี่ Row
     */
    override fun getItemCount(): Int {
        return list.size
    }

    /**
     * สำหรับดึงข้อมูลแต่ละ Row และ Set ให้ UI
     */
    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        val todo = list[position]
        holder.bind(todo)

        holder.itemView.setOnClickListener {
            onItemSelected(todo)
        }
    }
}