package th.ac.su.locallist.services

import android.content.Context
import android.content.Intent
import android.net.Uri
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream

open class ImageMultiPartCreatorLogin {
    fun createMultipart(context: Context, uri: Uri): MultipartBody.Part {
        val flag = Intent.FLAG_GRANT_READ_URI_PERMISSION
        context.contentResolver.takePersistableUriPermission(uri, flag)

        val iStream = context.contentResolver.openInputStream(uri)
        val byteBuffer = ByteArrayOutputStream()
        val bufferSize = 1024
        val buffer = ByteArray(bufferSize)

        var len = 0
        iStream?.let { stream ->
            while (stream.read(buffer).also { len = it } != -1) {
                byteBuffer.write(buffer, 0, len)
            }
        }

        val body = MultipartBody.Part.createFormData(
            partName, "app.jpg", RequestBody.create(
                MediaType.parse("image/*"), byteBuffer.toByteArray()
            )
        )

        return body
    }

    protected open val partName = "file"
}