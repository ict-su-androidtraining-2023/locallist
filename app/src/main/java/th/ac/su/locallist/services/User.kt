package th.ac.su.locallist.services

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

/**
 * Set Parcelize and implement Parcelable for can putExtra by class Data
 */
@Parcelize
class User(
    val id: Int = 0,
    @SerializedName("firstname") val firstName: String,
    val studentId: String,
    val profilePicture: String? = null,
    val tokenNotification: String = "1234567890"
) : Parcelable {
    companion object {
        @IgnoredOnParcel
        val CLASS_DATA_NAME = "UserData"
    }
}