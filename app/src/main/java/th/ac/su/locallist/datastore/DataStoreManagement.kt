package th.ac.su.locallist.datastore

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

class DataStoreManagement {

    val firstNameKey = stringPreferencesKey(FIRST_NAME)
    val studentIdKey = stringPreferencesKey(STUDENT_ID)
    val pictureProfileKey = stringPreferencesKey(PICTURE_PROFILE)


    // suspend function is set for run in background Thread
    suspend fun saveDataLogin(context: Context, firstName: String, studentId: String) {
        context.dataStore.edit { settings ->
            settings[firstNameKey] = firstName
            settings[studentIdKey] = studentId
        }
    }

    suspend fun saveDataPicture(context: Context, pictureLink: String) {
        context.dataStore.edit { settings ->
            settings[pictureProfileKey] = pictureLink
        }
    }

    suspend fun readDataLogin(
        context: Context,
        returnData: (firstName: String, studentId: String, picturePath: String?) -> Unit // return data by lambda function
    ) {
        val mapPreferences = context.dataStore.data.first()
        val firstName: String = mapPreferences[firstNameKey] ?: ""
        val studentId: String = mapPreferences[studentIdKey] ?: ""
        val picturePath: String = mapPreferences[pictureProfileKey] ?: ""

        returnData(firstName, studentId, picturePath)
    }
}