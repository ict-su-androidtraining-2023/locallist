package th.ac.su.locallist.extension

import android.app.Activity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import th.ac.su.locallist.R

fun Activity.showErrorAlertDialog(message: String) {
    // Check Activity is Running
    if (!isFinishing) {
        MaterialAlertDialogBuilder(this)
            .setMessage(message)
            .setPositiveButton(R.string.ok) { _, _ -> } // dialog dismiss auto, when click on button. in function {} not command anything for now
            .show()
    }

}