package th.ac.su.locallist.services

import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface Services {
    @POST("user")
    fun createOrGetUser(@Body user: User): Call<User>

    @Multipart
    @POST("user/upload/{userId}")
    fun updateProfilePicture(
        @Path("userId") userId: String,
        @Part partFile: MultipartBody.Part
    ): Call<User>

    @GET("todo")
    fun loadTodo(@Query("userid") userId: String): Call<List<Todo>>

    @POST("todo")
    fun createTodo(@Body data: Todo): Call<Todo>

    @PATCH("todo/{todoId}")
    fun updateTodo(@Path("todoId") todoId: String, @Body data: Todo): Call<Todo>

    @DELETE("todo/{todoId}")
    fun deleteTodo(@Path("todoId") todoId: String): Call<ResponseBody>

    @Multipart
    @POST("todo/upload/{todoId}")
    fun updateTodoPicture(
        @Path("todoId") todoId: String,
        @Part partFile: MultipartBody.Part
    ): Call<Todo>

}