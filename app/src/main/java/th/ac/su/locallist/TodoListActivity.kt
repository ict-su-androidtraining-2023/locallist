package th.ac.su.locallist

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.os.LocaleListCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import th.ac.su.locallist.databinding.ActivityTodoListBinding
import th.ac.su.locallist.extension.showErrorAlertDialog
import th.ac.su.locallist.services.RetrofitCreator
import th.ac.su.locallist.services.Todo
import th.ac.su.locallist.services.User

class TodoListActivity : AppCompatActivity() {

    private var user: User? = null
    private val binding: ActivityTodoListBinding by lazy {
        ActivityTodoListBinding.inflate(
            layoutInflater
        )
    }

    private var todoList: ArrayList<Todo> = arrayListOf()
    private val adapter = TodoAdapter(todoList) {
        val intent = Intent(this, AddTodoActivity::class.java)
        intent.putExtra(Todo.CLASS_DATA_NAME, it)
        intent.putExtra(User.CLASS_DATA_NAME, user)
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.recycler.adapter = adapter
        binding.recycler.layoutManager = LinearLayoutManager(this)

        setSupportActionBar(binding.materialToolbar)

        val itemDecoration: RecyclerView.ItemDecoration =
            DividerItemDecoration(this, RecyclerView.VERTICAL)
        binding.recycler.addItemDecoration(itemDecoration)

        binding.swipeRefresh.setOnRefreshListener {
            loadTodoList()
        }

        binding.fabAddTodo.setOnClickListener {
            val intent = Intent(this, AddTodoActivity::class.java)
            startActivity(intent)
        }

        binding.recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0 && binding.fabAddTodo.isExtended) {
                    binding.fabAddTodo.shrink()
                } else if (dy < 0 && !binding.fabAddTodo.isExtended) {
                    binding.fabAddTodo.extend()
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()

        binding.swipeRefresh.isRefreshing = true
        loadTodoList()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.todolist_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.language -> {
                changeLanguage()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun changeLanguage() {
        val lang = AppCompatDelegate.getApplicationLocales()

        val newLange = if (lang.toLanguageTags() == TH) {
            ENG
        } else {
            TH
        }

        val appLocale: LocaleListCompat = LocaleListCompat.forLanguageTags(newLange)
        // Call this on the main thread as it may require Activity.restart()
        AppCompatDelegate.setApplicationLocales(appLocale)

    }

    private fun loadTodoList() {

        user = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent.getParcelableExtra(User.CLASS_DATA_NAME, User::class.java)
        } else {
            intent.getParcelableExtra(User.CLASS_DATA_NAME)
        }

        if (user != null) {
            val userId = user!!.id
            RetrofitCreator.service.loadTodo("$userId").enqueue(object : Callback<List<Todo>> {
                override fun onResponse(call: Call<List<Todo>>, response: Response<List<Todo>>) {
                    if (binding.swipeRefresh.isRefreshing) {
                        binding.swipeRefresh.isRefreshing = false
                    }

                    if (response.isSuccessful) {
                        val list = response.body()
                        if (!list.isNullOrEmpty()) {
                            // hide TxtNoData
                            binding.txtNoData.visibility = View.GONE
                            refreshListData(list)
                        } else {
                            // un-hide TxtNoData
                            binding.txtNoData.visibility = View.VISIBLE
                        }

                    } else {
                        showErrorAlertDialog("Response Error: ${response.errorBody()}")
                    }
                }

                override fun onFailure(call: Call<List<Todo>>, t: Throwable) {
                    showErrorAlertDialog("Error ${t.localizedMessage}")
                    if (binding.swipeRefresh.isRefreshing) {
                        binding.swipeRefresh.isRefreshing = false
                    }
                }

            })
        } else {
            showErrorAlertDialog(getString(R.string.no_user_data))
        }


    }

    private fun refreshListData(list: List<Todo>) {
        todoList.clear()
        todoList.addAll(list)
        todoList.sortBy { it.id }
        /**
         *  for refresh recycler data list with new data
         *  P.S. this is old fashion way, you should know what position is change and use notifyItemChanged(position) for change by position
         */
        adapter.notifyDataSetChanged()
    }
}