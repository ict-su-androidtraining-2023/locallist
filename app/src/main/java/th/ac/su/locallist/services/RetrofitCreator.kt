package th.ac.su.locallist.services

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * ต้องย้าย Retrofit มาสร้างเป็น Single Instance เนื่องจาก หาก new Retrofit เยอะเกินไปจะทำให้ App Crash
 */
class RetrofitCreator {

    companion object {
        lateinit var service: Services

        fun createRetrofitService(baseUrl: String) {
            val okHttpClient: OkHttpClient = OkHttpClient.Builder()
                .callTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .build()

            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            service = retrofit.create(Services::class.java)
        }
    }
}