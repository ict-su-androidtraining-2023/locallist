package th.ac.su.locallist

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresPermission
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import com.bumptech.glide.Glide
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import th.ac.su.locallist.databinding.ActivityAddTodoBinding
import th.ac.su.locallist.databinding.ContentAddTodoBinding
import th.ac.su.locallist.extension.showErrorAlertDialog
import th.ac.su.locallist.services.ImageMultiPartCreatorTodo
import th.ac.su.locallist.services.RetrofitCreator
import th.ac.su.locallist.services.Todo
import th.ac.su.locallist.services.User

class AddTodoActivity : AppCompatActivity() {

    private val TAG: String? = AddTodoActivity::class.simpleName
    private var user: User? = null

    private val bindingLayout: ActivityAddTodoBinding by lazy {
        ActivityAddTodoBinding.inflate(
            layoutInflater
        )
    }

    private val binding: ContentAddTodoBinding by lazy {
        bindingLayout.content
    }

    private var location: String? = null
    private var imageUri: Uri? = null
    private var todoData: Todo? = null

    // registerForActivityResult ต้องกำหนดเป็น Global Variable เท่านั้น ไม่งั้นจะทำให้ App Crash
    private val pickPicture =
        registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
            // Callback is invoked after the user selects a media item or closes the
            // photo picker.

            uri?.let {
                // if uri is not null it working in this area
                setUriToImageView(it)
            }
        }

    private val fusedLocationClient: FusedLocationProviderClient by lazy {
        LocationServices.getFusedLocationProviderClient(
            this
        )
    }

    @SuppressLint("MissingPermission")
    private val locationPermissionRequest = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions ->
        when {
            permissions.getOrDefault(Manifest.permission.ACCESS_FINE_LOCATION, false) -> {
                requestLocationData()
            }

            permissions.getOrDefault(Manifest.permission.ACCESS_COARSE_LOCATION, false) -> {
                requestLocationData()
            }

            else -> {
                MaterialAlertDialogBuilder(this)
                    .setMessage(R.string.dialog_location_permission_not_allow)
                    .setPositiveButton(R.string.ok) { _, _ -> }
                    .show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(bindingLayout.root)
        setSupportActionBar(bindingLayout.toolbarAddTodo)

        bindingLayout.toolbarAddTodo.setNavigationOnClickListener {
            // set same back as physical back
            onBackPressedDispatcher.onBackPressed()
        }

        user = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent.getParcelableExtra(User.CLASS_DATA_NAME, User::class.java)
        } else {
            intent.getParcelableExtra(User.CLASS_DATA_NAME)
        }


        todoData = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent.getParcelableExtra(Todo.CLASS_DATA_NAME, Todo::class.java)
        } else {
            intent.getParcelableExtra(Todo.CLASS_DATA_NAME)
        }

        if (todoData != null) {
            supportActionBar?.title = getString(R.string.edit_data)

            binding.txfTodoHeader.editText?.setText(todoData!!.header)
            binding.txfTodoMessage.editText?.setText(todoData!!.message)
            todoData!!.locationName?.let {
                binding.txvLocation.text = it
            }

            todoData!!.picture?.let { picture ->
                val imageUrl = "${getString(R.string.base_url)}/$picture"
                binding.imvTodoGallery.scaleType = ImageView.ScaleType.FIT_CENTER
                Glide.with(this@AddTodoActivity)
                    .load(imageUrl)
                    .placeholder(R.drawable.ic_download)
                    .error(R.drawable.ic_error)
                    .fitCenter()
                    .into(binding.imvTodoGallery)
            }

            binding.btnSave.isEnabled = true
        } else {
            supportActionBar?.title = getString(R.string.add_data)
            binding.btnSave.isEnabled = false
        }


        binding.btnCancel.setOnClickListener {
            onBackPressedDispatcher.onBackPressed()
        }

        binding.imvLocation.setOnClickListener {
            getLocationData()
        }

        binding.imvTodoGallery.setOnClickListener {
            pickPicture.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
        }


        binding.btnSave.setOnClickListener {
            val header = binding.txfTodoHeader.editText?.text.toString()
            val message = binding.txfTodoMessage.editText?.text.toString()

            val data = Todo(
                userId = 1,
                header = header,
                message = message,
                picture = todoData?.picture,
                locationName = location
            )

            if (todoData == null) {
                createTodo(data)
            } else {
                updateTodo(data)
            }
        }

        binding.txfTodoHeader.editText?.addTextChangedListener {
            checkEnableSave()
        }
        binding.txfTodoMessage.editText?.addTextChangedListener {
            checkEnableSave()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (todoData != null) {
            val inflater: MenuInflater = menuInflater
            inflater.inflate(R.menu.addtodo_menu, menu)
            return true
        } else {
            return false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.delete -> {
                deleteTodo()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun deleteTodo() {
        todoData?.let {
            bindingLayout.loading.root.visibility = View.VISIBLE
            RetrofitCreator.service.deleteTodo(it.id.toString())
                .enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(
                        call: Call<ResponseBody>,
                        response: Response<ResponseBody>
                    ) {
                        bindingLayout.loading.root.visibility = View.GONE
                        if (response.isSuccessful) {
                            finish()
                        } else {
                            showErrorAlertDialog("Error: ${response.errorBody()?.string()}")
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        bindingLayout.loading.root.visibility = View.GONE
                        showErrorAlertDialog("Failure: ${t.localizedMessage}")
                    }

                })
        }
    }

    private fun updateTodo(data: Todo) {
        todoData?.let {

            bindingLayout.loading.root.visibility = View.VISIBLE
            RetrofitCreator.service.updateTodo(it.id.toString(), data)
                .enqueue(object : Callback<Todo> {
                    override fun onResponse(call: Call<Todo>, response: Response<Todo>) {
                        if (response.isSuccessful) {
                            response.body()?.let { todoCreated ->
                                uploadImage(todoCreated)
                            }
                        } else {
                            bindingLayout.loading.root.visibility = View.GONE
                            showErrorAlertDialog("Error: ${response.errorBody()?.string()}")
                        }
                    }

                    override fun onFailure(call: Call<Todo>, t: Throwable) {
                        bindingLayout.loading.root.visibility = View.GONE
                        showErrorAlertDialog("Failure: ${t.localizedMessage}")
                    }

                })
        }
    }

    private fun createTodo(data: Todo) {
        bindingLayout.loading.root.visibility = View.VISIBLE
        RetrofitCreator.service.createTodo(data).enqueue(object : Callback<Todo> {
            override fun onResponse(call: Call<Todo>, response: Response<Todo>) {
                if (response.isSuccessful) {
                    response.body()?.let { todoCreated ->
                        uploadImage(todoCreated)
                    }
                } else {
                    bindingLayout.loading.root.visibility = View.GONE
                    showErrorAlertDialog("Error: ${response.errorBody()?.string()}")
                }
            }

            override fun onFailure(call: Call<Todo>, t: Throwable) {
                bindingLayout.loading.root.visibility = View.GONE
                showErrorAlertDialog("Failure: ${t.localizedMessage}")
            }

        })
    }

    private fun uploadImage(todoCreated: Todo) {
        if (imageUri != null) {
            val body = ImageMultiPartCreatorTodo().createMultipart(this, imageUri!!)
            RetrofitCreator.service.updateTodoPicture(todoId = todoCreated.id.toString(), body)
                .enqueue(object : Callback<Todo> {
                    override fun onResponse(
                        call: Call<Todo>,
                        response: Response<Todo>
                    ) {
                        bindingLayout.loading.root.visibility = View.GONE
                        Log.d(TAG, "Upload onResponse${response.code()}")
                        finish() // finish this Activity
                    }

                    override fun onFailure(call: Call<Todo>, t: Throwable) {
                        bindingLayout.loading.root.visibility = View.GONE
                        showErrorAlertDialog("Upload onFailure: ${t.localizedMessage}")
                    }
                })
        } else {
            bindingLayout.loading.root.visibility = View.GONE
            finish()
        }
    }

    private fun checkEnableSave() {
        val header = binding.txfTodoHeader.editText?.text.toString()
        val message = binding.txfTodoMessage.editText?.text.toString()
        val isEnabled = !(header.isEmpty() || message.isEmpty())
        binding.btnSave.isEnabled = isEnabled
    }

    private fun getLocationData() {
        val grantFineLocation = ContextCompat.checkSelfPermission(
            this, Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED

        val grantCoarseLocation = ContextCompat.checkSelfPermission(
            this, Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED

        val shouldShowDialog =
            shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)
                    || shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)

        when {
            grantFineLocation || grantCoarseLocation -> {
                requestLocationData()
            }

            shouldShowDialog -> {
                MaterialAlertDialogBuilder(this)
                    .setMessage(getString(R.string.dialog_location_permission))
                    .setPositiveButton(R.string.setting) { _, _ ->
                        gotoSetting()
                    }.show()
            }

            else -> {
                locationPermissionRequest.launch(
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    )
                )
            }
        }
    }

    @RequiresPermission(
        anyOf = [Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION]
    )
    private fun requestLocationData() {
        val isDeviceHaveGooglePlay =
            GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)

        if (isDeviceHaveGooglePlay == ConnectionResult.SUCCESS) {
            fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
                location?.let {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                        Geocoder(this).getFromLocation(it.latitude, it.longitude, 1) {
                            val result = it.firstOrNull()
                            result?.let {
                                getStringAddress(it)
                            }
                        }
                    } else {
                        CoroutineScope(Dispatchers.IO).launch {
                            val resultList = Geocoder(this@AddTodoActivity).getFromLocation(
                                it.latitude,
                                it.longitude,
                                1
                            )
                            withContext(Dispatchers.Main) {
                                val result = resultList?.firstOrNull()
                                result?.let {
                                    getStringAddress(it)
                                }
                            }
                        }
                    }
                }
            }
        } else {
            MaterialAlertDialogBuilder(this)
                .setMessage(R.string.device_not_have_google_play)
                .setPositiveButton(R.string.ok) { _, _ ->

                }.show()
        }
    }

    private fun getStringAddress(result: Address) {
        CoroutineScope(Dispatchers.Main).launch {
            var address = ""
            for (line in 0..result.maxAddressLineIndex) {
                address += "${result.getAddressLine(line)}"
            }
            setAddressText(address)
        }
    }

    private fun setAddressText(address: String) {
        location = address
        binding.txvLocation.text = location
    }

    private fun gotoSetting() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivity(intent)
    }

    private fun setUriToImageView(uri: Uri) {
        imageUri = uri
        binding.imvTodoGallery.scaleType = ImageView.ScaleType.FIT_CENTER

        Glide.with(this@AddTodoActivity)
            .load(imageUri)
            .placeholder(R.drawable.ic_download)
            .error(R.drawable.ic_error)
            .fitCenter()
            .into(binding.imvTodoGallery)
    }

    private val LOCATION_KEY = "location"
    private val PICTURE_KEY = "picture"
    private val TODO_KEY = "todo"

    private val HEADER_TEXT_KEY = "header"
    private val MESSAGE_TEXT_KEY = "message"

    /**
     * เนื่องจากตัวแปรที่กำหนดใน Activity จะถูกทำลายหากมีการพลิกหน้าจอ
     * ต้องทำการเก็บ State และคืนค่าใหม่
     */
    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(LOCATION_KEY, location)
        outState.putParcelable(PICTURE_KEY, imageUri)
        outState.putParcelable(TODO_KEY, todoData)


        val header = binding.txfTodoHeader.editText?.text.toString()
        val message = binding.txfTodoMessage.editText?.text.toString()
        outState.putString(HEADER_TEXT_KEY, header)
        outState.putString(MESSAGE_TEXT_KEY, message)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        val location = savedInstanceState.getString(LOCATION_KEY)
        location?.let {
            setAddressText(it)
        }

        imageUri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            savedInstanceState.getParcelable(PICTURE_KEY, Uri::class.java)
        } else {
            savedInstanceState.getParcelable(PICTURE_KEY)
        }
        imageUri?.let { setUriToImageView(it) }
        todoData = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            savedInstanceState.getParcelable(TODO_KEY, Todo::class.java)
        } else {
            savedInstanceState.getParcelable(TODO_KEY)
        }

        val header = savedInstanceState.getString(HEADER_TEXT_KEY)
        if (!header.isNullOrEmpty()) {
            binding.txfTodoHeader.editText?.setText(header)
        }

        val message = savedInstanceState.getString(MESSAGE_TEXT_KEY)
        if (!message.isNullOrEmpty()) {
            binding.txfTodoMessage.editText?.setText(message)
        }

    }
}