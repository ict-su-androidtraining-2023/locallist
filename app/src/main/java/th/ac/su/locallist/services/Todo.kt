package th.ac.su.locallist.services

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
class Todo(
    val id: Int = 0,
    val userId: Int,
    var header: String,
    @SerializedName("bodyMessage") var message: String?,
    val picture: String? = null,
    var locationName: String? = null
) : Parcelable {
    companion object {

        @IgnoredOnParcel
        val CLASS_DATA_NAME = "TodoData"
    }
}
