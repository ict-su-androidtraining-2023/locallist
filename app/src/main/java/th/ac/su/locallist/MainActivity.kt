package th.ac.su.locallist

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.os.LocaleListCompat
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import th.ac.su.locallist.databinding.ActivityMainBinding
import th.ac.su.locallist.databinding.ComponentMainBinding
import th.ac.su.locallist.datastore.DataStoreManagement
import th.ac.su.locallist.extension.md5
import th.ac.su.locallist.extension.showErrorAlertDialog
import th.ac.su.locallist.services.ImageMultiPartCreatorLogin
import th.ac.su.locallist.services.RetrofitCreator
import th.ac.su.locallist.services.User

class MainActivity : AppCompatActivity() {

    private val TAG = MainActivity::class.simpleName
    private val bindingLayout: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(
            layoutInflater
        )
    }
    private val binding: ComponentMainBinding by lazy { bindingLayout.contentMain }

    private var imageUri: Uri? = null
    private val dataStore by lazy { DataStoreManagement() }

    // registerForActivityResult ต้องกำหนดเป็น Global Variable เท่านั้น ไม่งั้นจะทำให้ App Crash
    private val pickPicture =
        registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
            // Callback is invoked after the user selects a media item or closes the
            // photo picker.

            uri?.let {
                // if uri is not null it working in this area
                setUriToImageView(it)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(bindingLayout.root)

        lifecycleScope.launch {
            dataStore.readDataLogin(this@MainActivity) { firstName, studentId, picture ->
                binding.txfFirstname.editText?.setText(firstName)
                binding.txfStudentId.editText?.setText(studentId)

                if (!picture.isNullOrEmpty()) {
                    val imageUrl = "${getString(R.string.base_url)}/$picture"
                    binding.imvGallery.scaleType = ImageView.ScaleType.FIT_XY
                    Glide.with(this@MainActivity)
                        .load(imageUrl)
                        .placeholder(R.drawable.ic_download)
                        .error(R.drawable.ic_error)
                        .fitCenter()
                        .into(binding.imvGallery)
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()

        binding.btnLogin.setOnClickListener {

            val studentId = binding.txfStudentId.editText?.text.toString()
            val user = User(
                firstName = binding.txfFirstname.editText?.text.toString(),
                studentId = studentId,
                tokenNotification = studentId.md5()
            )

            loginOrCreateUser(user)
        }

        binding.imvGallery.setOnClickListener {
            pickPicture.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
        }

        val lang = AppCompatDelegate.getApplicationLocales()

        if (lang.toLanguageTags() == TH) {
            binding.rdTh.isChecked = true
        } else {
            binding.rdEng.isChecked = true
        }

        binding.rdgroupLanguage.setOnCheckedChangeListener { group, checkedId ->
            val isThai = checkedId == binding.rdTh.id
            changeLanguage(isThai)
        }
    }

    private fun changeLanguage(isThai: Boolean) {
        val newLange = if (isThai) {
            TH
        } else {
            ENG
        }

        val appLocale: LocaleListCompat = LocaleListCompat.forLanguageTags(newLange)
        // Call this on the main thread as it may require Activity.restart()
        AppCompatDelegate.setApplicationLocales(appLocale)

    }

    private fun loginOrCreateUser(user: User) {
        bindingLayout.loading.root.visibility = View.VISIBLE
        RetrofitCreator.service.createOrGetUser(user).enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                bindingLayout.loading.root.visibility = View.GONE
                if (response.isSuccessful) {
                    response.body()?.let { userResponse ->
                        // Simple Way use Coroutine Background Thread
                        lifecycleScope.launch {
                            dataStore.saveDataLogin(
                                this@MainActivity,
                                userResponse.firstName,
                                userResponse.studentId
                            )

                            if (!userResponse.profilePicture.isNullOrEmpty()) {
                                dataStore.saveDataPicture(
                                    this@MainActivity,
                                    userResponse.profilePicture
                                )
                            }

                        }

                        uploadImage(userResponse.id)

                        gotoTodoListActivity(userResponse)
                    }
                } else {
                    showErrorAlertDialog("Response Error: ${response.errorBody()?.string()}")
                }
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                bindingLayout.loading.root.visibility = View.GONE
                showErrorAlertDialog("Error: ${t.localizedMessage}")
            }

        })
    }

    private fun gotoTodoListActivity(user: User) {
        val intent = Intent(this, TodoListActivity::class.java)
        intent.putExtra(User.CLASS_DATA_NAME, user)
        startActivity(intent)
        finish() // set for back from list not show Login Page Again
    }

    private fun uploadImage(userId: Int) {
        // check imageUri not null
        imageUri?.let { uri ->

            val body = ImageMultiPartCreatorLogin().createMultipart(this, uri)

            RetrofitCreator.service.updateProfilePicture(userId = userId.toString(), body)
                .enqueue(object : Callback<User> {
                    override fun onResponse(
                        call: Call<User>,
                        response: Response<User>
                    ) {
                        Log.d(TAG, "Upload onResponse${response.code()}")
                        if (response.isSuccessful) {
                            val user = response.body()
                            user?.profilePicture?.let {
                                CoroutineScope(Dispatchers.IO).launch {
                                    // add path folder because cannot return in webservice
                                    dataStore.saveDataPicture(this@MainActivity, it)
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<User>, t: Throwable) {
                        Log.d(TAG, "Upload onFailure: ${t.localizedMessage}")
                    }
                })

        }
    }

    private fun setUriToImageView(uri: Uri) {
        imageUri = uri
        binding.imvGallery.scaleType = ImageView.ScaleType.FIT_XY
        Glide.with(this@MainActivity)
            .load(imageUri)
            .placeholder(R.drawable.ic_download)
            .error(R.drawable.ic_error)
            .fitCenter()
            .into(binding.imvGallery)
    }

    private val PICTURE_KEY = "picture"
    private val FIRSTNAME_KEY = "first"
    private val STUDENT_KEY = "student"

    /**
     * เนื่องจากตัวแปรที่กำหนดใน Activity จะถูกทำลายหากมีการพลิกหน้าจอ
     * ต้องทำการเก็บ State และคืนค่าใหม่
     */
    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(PICTURE_KEY, imageUri)

        val firstname = binding.txfFirstname.editText?.text.toString()
        val student = binding.txfStudentId.editText?.text.toString()
        outState.putString(FIRSTNAME_KEY, firstname)
        outState.putString(STUDENT_KEY, student)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        imageUri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            savedInstanceState.getParcelable(PICTURE_KEY, Uri::class.java)
        } else {
            savedInstanceState.getParcelable("picture")
        }
        imageUri?.let { setUriToImageView(it) }

        val firstname = savedInstanceState.getString(FIRSTNAME_KEY)
        val student = savedInstanceState.getString(STUDENT_KEY)
        if (!firstname.isNullOrEmpty()) {
            binding.txfFirstname.editText?.setText(firstname)
        }
        if (!student.isNullOrEmpty()) {
            binding.txfStudentId.editText?.setText(student)
        }
    }


}














