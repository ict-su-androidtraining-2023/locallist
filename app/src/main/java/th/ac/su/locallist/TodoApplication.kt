package th.ac.su.locallist

import android.app.Application
import th.ac.su.locallist.services.RetrofitCreator

class TodoApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        RetrofitCreator.createRetrofitService(getString(R.string.base_url))
    }

}